import React from 'react';
import {shallow} from 'enzyme';
import ProcessData from '../searchForm/component/ProcessData.react';

test('testing the DOM of the application for text', () => {
  // Render the DOM of the application
  const searchForm = shallow(
    <ProcessData />
  );

  expect(searchForm.text()).toContain('Search For A Fruit - Enter a text to get fruit names that have that text in them (Case Insensitive)');


});

test('testing the DOM for a single form element', () => {
  // Render the application DOM
  const searchForm = shallow(
    <ProcessData />
  );

  expect(searchForm.find('form')).toHaveLength(1);

});

test('testing the DOM for a child element called DisplayList', () => {
  // Render the application DOM
  const searchForm = shallow(
    <ProcessData />
  );

  expect(searchForm.find('DisplayList')).toHaveLength(1);


});
