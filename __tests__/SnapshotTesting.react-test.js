import React from 'react';
import ProcessData from '../searchForm/component/ProcessData.react';
import renderer from 'react-test-renderer';

test('testing if Application rendered properly', () => {
  const component = renderer.create(
    <ProcessData/>
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

});
